
evitelaprison
=============

A Symfony project created on July 21, 2017, 10:27 am.
=======
# README #


### Contexte ###

evitelaprison.io est un site pour comprendre comment je peux choisir et utiliser des librairies, API, applications pour mon projet : mes obligations légales quand j’utilise une ressource existante dans mon travail.

### Storytelling ###

Jack est un entrepreneur qui a échoué dans son précédent projet UberLowCost à cause de soucis juridiques. Il veut maintenant aider les personnes comme lui, qui ne s’y connaissent pas en droit mais qui ont des responsabilités dans leur projet. Il souhaite faire prendre conscience des enjeux du juridique dans le web, proposer un outil pour trouver rapidement de l’information, qui soit collaboratif et même où l’on peut acheter une review sur notre cahier des charges techniques.

