<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogPost;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function listAction()
    {
        $blogposts = $this->getDoctrine()
        ->getRepository('AppBundle:BlogPost') //récupère le dépot
        ->findAll();

        return $this->render('blog/index.html.twig', array(
            'blogpost' => $blogposts
        ));
    }

    /**
     * @Route("blog/create", name="art_create")
     */
    public function createAction(Request $request)
    {
        $blogposts = $this->getDoctrine()
            ->getRepository('AppBundle:BlogPost') //récupère le dépot
            ->findAll();

        return $this->render('blog/create_blog.html.twig', array(
            'blogpost' => $blogposts
        ));
    }

    /**
     * @Route("blog/edit/{id}", name="art_edit")
     */
    public function editAction($id, Request $request)
    {
        $blogposts = $this->getDoctrine()
            ->getRepository('AppBundle:BlogPost') //récupère le dépot
            ->findAll();

        return $this->render('blog/edit_blog.html.twig', array(
            'blogpost' => $blogposts
        ));
    }

}